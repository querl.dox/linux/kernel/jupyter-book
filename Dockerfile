FROM registry.gitlab.com/querl.dox/linux/kernel/jupyter-book:v0001


# create user with a home directory
ARG NB_USER
ARG NB_UID
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
    

COPY . ${HOME}

WORKDIR ${HOME}

RUN git config --global credential.helper cache && \
    git config --global user.email querl.dox.protonmail.com && \
    git config --global user.name querl.dox && \
    chmod +x clone.sh && \
    chmod +x push.sh

USER root

RUN chown -R ${NB_UID} ${HOME}

USER ${USER}
